'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:DiscussionController
 * @description
 * # DiscussionController
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('DiscussionController', [
    '$scope',
    '$routeParams',
    function ( $scope, $routeParams ) {
        $scope.mine = {
            name: 'Timothé',
            urlAvatar: 'https://en.gravatar.com/userimage/124160304/f94201100df736f923533bf18df8e465.jpg?size=40'
        };

        $scope.items = [
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Manon Gauthier'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'Le Village, 07160 Saint-Jean-Roure',
                        'coordinates': [
                            4.428030,
                            44.940539
                        ]
                    },
                    'date': 'February 08, 2018 09:15:00',
                },
                'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:35:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Timéo Brun'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Chèze, 07160 Le Cheylard',
                        'coordinates': [
                            4.430715,
                            44.906158
                        ]
                    },
                    'date': 'February 08, 2018 8:30:00',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:00:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
            {
                '_id': '5a782dc0e5882208022c6808',
                'user': {
                    'id': '5a782dc07d3bf1a696d7f376',
                    'name': 'Jean-Bernard Huet'
                },
                'journey': {
                  'start': {
                    'location': {
                        'name': 'La Brugière, 07190 Saint-Sauveur-de-Montagut',
                        'coordinates': [
                            4.582509,
                            44.820274
                        ]
                    },
                    'date': 'February 08, 2018 09:15:00',
                  },
                  'end': {
                    'location': {
                        'name': 'Privas 07000',
                        'coordinates': [
                            4.599039,
                            44.735269
                        ]
                    },
                    'date': 'February 08, 2018 10:15:00',
                    'time': {
                      'hour': 14,
                      'minute': 21
                    }
                  }
                }
            },
        ];

        $scope.his = {
            name: $scope.items[ $routeParams.idDiscussion ].user.name,
            urlAvatar: 'https://pbs.twimg.com/profile_images/1488446374/JB3.jpg'
        };
        
        
        
        
        $scope.msg = [
            {
                isMine: true,
                msg: 'Bonjour,<br>Je me permet de vous contacter suite à votre annonce concernant un trajet de "La Brugière, 07190 Saint-Sauveur-de-Montagut" à "Privas 07000".<br>Je compte également faire ce trajet, mais je ne suis pas véhiculé. Peut être pourrions nous voyager ensemble ?<br>',
                date: new Date('February 07, 2018 17:30:00'),
            },
            {
                isMine: false,
                msg: 'Bonjour Timothé,<br>Il n\'y a pas de soucis, on se donne rdv à 9h15 sur le parking devant la pompe à essence du village ?',
                date: new Date('February 07, 2018 18:05:00'),
            },
            {
                isMine: true,
                msg: 'Ça me va.<br>Merci beaucoup, à demain matin<br>Bonne soirée',
                date: new Date('February 07, 2018 18:07:00'),
            },
        ];
        
        if( $routeParams.idDiscussion === '0' ){
            $scope.his.urlAvatar = 'https://via.placeholder.com/40x40';
            $scope.msg = [
                {
                    isMine: false,
                    msg: 'Bonjour,<br>Je me permet de vous contacter suite à votre annonce concernant un trajet de "La Brugière, 07190 Saint-Sauveur-de-Montagut" à "Privas 07000".<br>Je suis conducteur et compte également faire ce trajet. Peut être pourrions nous voyager ensemble ?<br>N\'hésitez pas à me répondre de manière à discuter ensemble du point de rendez-vous.',
                    date: new Date('February 07, 2018 17:30:00'),
                },
            ];
        }
        
        $scope.hasMsg = ( $scope.msg.lenght !== 0 );
        
        console.log( $scope.hasMsg );

        $scope.sendMessage = function() {
            $scope.msg.push({
                isMine: true,
                date: new Date(),
                msg: $scope.newMessage
            });
            $scope.newMessage = '';
            $scope.hasMsg = true;
            console.log( $scope.msg );
            if(!$scope.$$phase) {
                $scope.$apply();
            }
        };
    }
]);
