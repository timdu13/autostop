'use strict';

angular.module( 'autostopApp' )
.controller( 'NavigationController', [
    '$scope',
    'AuthService',
    '$uibModal',
    '$location',
    function( $scope, AuthService, $uibModal, $location ) {
        var updateSelectedNav = function() {
            $scope.selected = {
                'index': '',
                'about': '',
                'inscription': ''
            };
            var debug = '';
            switch ( $location.url() ) {
                case '/':
                    debug = 'index';
                    $scope.selected.index = 'active';
                break;
                case '/about':
                    debug = 'about';
                    $scope.selected.about = 'active';
                break;
                case '/inscription':
                    debug = 'inscription';
                    $scope.selected.inscription = 'active';
                break;
            
                default:
                    break;
            }
        };
        updateSelectedNav();

        $scope.$on( '$locationChangeSuccess', function() {
            updateSelectedNav();
        });

        // Initialisation de l'espace membre
        $scope.isConnected = false;
        var isLogged = AuthService.isLogged();
        if( isLogged !== false ) {
            $scope.user = {
                'id': isLogged.id,
                'name': isLogged.name
            };

            $scope.isConnected = true;
        }

        $scope.openModal = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'connectionModalContent.html',
                controller: 'ConnectionModalInstanceController',
                
            });
        
            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.logout = function() {
            AuthService.logout();
            $scope.isConnected = false;
        };
    }
])
.controller( 'ConnectionModalInstanceController', [
    '$scope',
    'AuthService',
    '$uibModalInstance',
    function($scope, AuthService, $uibModalInstance) {
        $scope.user = {
            'email': '',
            'password': ''
        };
        $scope.modal = {};

        $scope.modalConnection = function() {
            if( AuthService.login( $scope.modal.email, $scope.modal.password ) === true ) {
                $uibModalInstance.close();
                window.location.reload();
            } else {
                angular.element( '.form-group' ).addClass( 'has-error' );
                angular.element( '#error-form' ).removeClass( 'hide' );
            }
        };

        $scope.modalCancel = function() {
            $uibModalInstance.close();
        };

        $scope.modalKeypressPassword = function( e ) {
            if( e.keyCode === 13 ) {
                $scope.modalConnection();
            }
        };
    }
]);