'use strict';

/**
 * @ngdoc function
 * @name autostopApp.controller:MestrajetsCtrl
 * @description
 * # MestrajetsCtrl
 * Controller of the autostopApp
 */
angular.module('autostopApp')
.controller('MestrajetsController', [
    '$scope',
    function ( $scope ) {

        $scope.journeys = [
            {
                id: 1,
                nbPlaces: 3,
                start: {
                    city: 'Le Cheylard',
                    date: new Date( 'February 08, 2018 08:00:00' ),
                },
                end: {
                    date: new Date( 'February 08, 2018 09:30:00' ),
                    city: 'Privas'
                }
            },
            {
                id: 2,
                nbPlaces: 0,
                start: {
                    date: new Date( 'February 08, 2018 14:00:00' ),
                    city: 'Privas'
                },
                end: {
                    date: new Date( 'February 08, 2018 15:30:00' ),
                    city: 'Le Cheylard'
                }
            }
        ];

        $scope.nbPlacesDanger = function( nbPlaces ) {
            var retour = '';
            if( nbPlaces === 0 ) {
                retour = 'badge-danger';
            }
            return retour;
        };

        $scope.formatDate = function( value ) {
            if( value < 10 ) {
                value = '0' + value;
            }

            return value;
        };

        console.log( $scope.journeys );
    }
]);
